You must set the following environment variables in your project in order to have the project work.
DEVHUB_AUTH_URL - AuthURL from SFDX CLI for your devhub org.
PACKAGING_AUTH_URL - AuthURL from SFDX CLI for your packaging org.
CUMULUSCI_KEY - Random 16-byte string, used to allow CLI tools to preserve state between jobs.
DBUS_PASS - Random string, used for authenticating requests to keychain.